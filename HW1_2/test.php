<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Checkbox Form</title>
</head>
<body>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (!empty($_POST['fruits'])) {
            $fruits = $_POST['fruits'];
            echo "You have selected: " . implode(", ", $fruits);
        } else {
            $fruits = array();
            echo "No fruits selected.";
        }
    } else {
        $fruits = array();
    }
    ?>
    <form action="" method="post">
        <label>
            <input type="checkbox" name="fruits[]" value="Apple" <?php if(in_array("Apple", $fruits)) echo "checked"; ?>>
            Apple
        </label>
        <label>
            <input type="checkbox" name="fruits[]" value="Banana" <?php if(in_array("Banana", $fruits)) echo "checked"; ?>>
            Banana
        </label>
        <label>
            <input type="checkbox" name="fruits[]" value="Cherry" <?php if(in_array("Cherry", $fruits)) echo "checked"; ?>>
            Cherry
        </label>
        <input type="submit" value="Submit">
    </form>

</body>
</html>
