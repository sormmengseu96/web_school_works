<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
  <style>
    .container {
      position: relative;
      align-items: center;
      justify-items: center;
      top: 20px;
    }
   input[type="checkbox"]{
      font-size: 22px;
      position: relative;
      left: 10px;
   }
  </style>
</head>

<body>

  <?php
  if ($_SERVER['REQUEST_METHOD'] == "POST") {
      
    $product = $_POST['product_name'];
    $type_of_product = gettype($product);

    $price = $_POST['price'];
    $va = $price * 1;
    $type_of_price = gettype($va);

    $qty = $_POST['qty'];
    $va = (int)$qty;
    $type_of_qty = gettype($va);

    $type_of = "";
    $choose_product = $_POST['type_of_product'];
    $bol = (bool)$choose_product;
    $bol == 1 ? $type_of = "true" : $type_of = "false";

    $dec = $_POST['decription'];
    $type_of_dis = gettype($dec);

    if (isset($_POST["dis"])) {
      $discount_percent= $_POST["dis"];
      $type_of_discount = gettype($discount_percent);
    } else {
      $type_of_discount = 'no select'; 
    }
  }
  ?>
  <div class="container">
    <div class="card border-dark">
      <form method="post">
        <div class="card-header">
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-6">
              <label for="inputEmail4" class="form-label">Product name</label>
              <input type="text" class="form-control border-dark" name="product_name">
            </div>
            <div class="col-md-6">
              <label for="inputPassword4" class="form-label">Price</label>
              <input type="number" step="any" class="form-control border-dark" name="price">
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <label for="inputEmail4" class="form-label">Quantity</label>
              <input type="number" class="form-control border-dark" name="qty">
            </div>
            <div class="col-md-6">
              <label for="inputPassword4" class="form-label">type of product</label>
              <select class="form-select border-dark" name="type_of_product">
                <option value="1">good</option>
                <option value="0">bad</option>
              </select>
            </div>
          </div>
          <div class="col-12">
            <label for="inputAddress" class="form-label">Decription</label>
            <textarea type="text" class="form-control border-dark" name="decription"></textarea>
          </div>
          <div class="col-12">
            <div class="col-md-6">
              <label for="inputPassword4" class="form-label">Discount</label>
              <div class="form-check">
                <input class="form-check-input border-dark" type="checkbox" value="10" id="flexCheckDefault" name="dis[]">
                <label class="form-check-label" for="flexCheckDefault">
                  10%
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input border-dark" type="checkbox" value="20" id="flexCheckChecked" name="dis[]">
                <label class="form-check-label" for="flexCheckChecked">
                  20%
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input border-dark" type="checkbox" value="30" id="flexCheckChecked" name="dis[]">
                <label class="form-check-label" for="flexCheckChecked">
                  30%
                </label>
              </div>
            </div>
          </div>
        </div>
        <div class="card-footer">
          <button class="btn btn-dark" type="submit">submit</button>
        </div>
      </form>
    </div>
    <div class="card border-dark" style="margin-top: 20px;">
      <table class="table">
        <thead>
          <tr>
            <th scope="col">Detail</th>
            <th scope="col">Value</th>
            <th scope="col">Return Data type</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>ProductName</td>
            <td><?php echo $product ?></td>
            <td><?php echo $type_of_product ?></td>
          </tr>
          <tr>
            <td>Pirce</td>
            <td><?php echo $price ?></td>
            <td><?php echo $type_of_price ?></td>
          </tr>
          <tr>
            <td>Qunitity</td>
            <td><?php echo $qty ?></td>
            <td><?php echo $type_of_qty ?></td>
          </tr>
          <tr>
            <td>Type of product</td>
            <td><?php echo $choose_product ?></td>
            <td><?php echo $type_of ?></td>
          </tr>
          <tr>
            <td>Message</td>
            <td><?php echo $dec ?></td>
            <td><?php echo $type_of_dis ?></td>

          </tr>
          <tr>
            <td scope="row">Option Discount</td>
            <td>
              <?php 
                foreach($discount_percent as $discount){
                  echo $discount."%,";
                } 
              ?>
            </td>
            <td><?php echo $type_of_discount ?></td>
          </tr>
        </tbody>
      </table>
    </div>

</body>

</html>