<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <title>Document</title>
    <style>
        body {
            font-family: Battambang;
        }

        .container {
            align-items: center;
            justify-content: center;
            display: flex;
            top: 20px;
            position: relative;
        }

        .card {
            width: 600px;
            height: 25rem;
        }
    </style>
</head>

<body>
    <?php
        $e = "";$lists=[];$principal=$term_in_Year=$month=$year=$interest_Rate=$rate=$monthlyPayment=$totalpayment=$principal=0;
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $principal = $_POST['principal'];
            $term_in_Year = $_POST['month'];
            $interest_Rate = $_POST['rate'];
            $year = $_POST['year'];
            if(empty($principal) || empty($term_in_Year) || empty($interest_Rate)){
                $e = "<label style='color:red'>*is valid!</label>";
            } else {
                function cal($principal,$term_in_Year,$interest_Rate){
                    $monthlyInterestRate = $interest_Rate /12/100;
                    $numPayment = $term_in_Year;
                    $monthlyPayment = $principal * $monthlyInterestRate * pow(1 + $monthlyInterestRate,$numPayment)/(pow(1 + $monthlyInterestRate,$numPayment) -1);
                    return $monthlyPayment;
                }
                function generateSchedule($principal,$term_in_Year,$interest_Rate){
                    $monthlyPayment = cal($principal,$term_in_Year,$interest_Rate);
                    $balance = $principal;
                    $monthlyInterestRate = $interest_Rate/12/100;
                    $schedule = [];
                    $total = 0;
                    for ($month = 1; $month <= $term_in_Year ; $month++) {
                        $interestPayment = $balance * $monthlyInterestRate;
                        $principalPayment = $monthlyPayment - $interestPayment;
                        $balance -= $principalPayment;
                        $total +=$interestPayment;
    
                    $schedule[] = [
                        'month' => $month,
                        'payment' => round($monthlyPayment, 2),
                        'principal' => round($principalPayment, 2),
                        'interest' => round($interestPayment, 2),
                        'balance' => round($balance, 2),
                        ];
                    }
                    return ['schedule' => $schedule,'totalpayment' => round($total,2),'Total' => round($monthlyPayment,2)];
                }
                $result = generateSchedule($principal,$term_in_Year,$interest_Rate);
                $lists = $result['schedule'];
                $totalpayment = $result['totalpayment'];
                $monthlyPayment = $result['Total'];
            }
            
        }
    ?>
    <div class="container">
        <div class="row">
            <?php echo $e?>
            <div class="col">
                <div class="card">
                    <form method="post">
                        <div class="card border-dark">
                            <div class="card-body ">
                                <div class="col-md-12">
                                    <label for="inputEmail3" class="col-sm-2 col-form-label">💵ឥណទាន</label>
                                    <div class="input-group">
                                        <div class="input-group-text">$</div>
                                        <input type="number" class="form-control" id="principal" name="principal" value="<?php echo $principal?>">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label for="inputPassword3" class="col-sm-5 col-form-label">📅រយះពេលនៃការខ្ចីគិតជាឆ្នាំ</label>
                                    <input type="number" class="form-control" id="year" name="year" value="<?php echo $year?>">
                                </div>
                                <div class="col-md-12">
                                    <label for="inputPassword3" class="col-sm-5 col-form-label">📅រយះពេលនៃការខ្ចីគិតជាខែ</label>
                                    <input type="number" class="form-control" id="month" name="month" value="<?php echo $term_in_Year?>">
                                </div>
                                <div class="col-md-12">
                                    <label for="inputPassword3" class="col-sm-5 col-form-label">📅អត្រាការប្រាក់ប្រចាំឆ្នាំ</label>
                                    <div class="input-group">
                                        <div class="input-group-text">%</div>
                                        <input type="number" step="any" class="form-control" id="rate" name="rate" value="<?php echo $interest_Rate?>">
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-dark" style="--bs-btn-padding-y: .25rem; --bs-btn-padding-x: .5rem; --bs-btn-font-size: .75rem;">គណនា</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col">
                <div class="card border-dark">
                    <div class="card-header">
                        <h5 style="text-align: center;">ការប្រាក់ប្រចាំខែ</h5>
                    </div>
                    <div class="card-body">
                        <div class="Payment" style="text-align: center;top: 50px;position: relative;">
                            <h1>
                                <?php echo "$".$monthlyPayment?>
                            </h1>
                        </div>
                        <table class="table" style="position: relative;top:115px;">
                            <tr>
                                <th>ប្រាក់ដើមសរុបដែលត្រូវបង់:</th>
                                <td><?php echo "$". $principal?></td>
                            </tr>
                            <tr>
                                <th>ការប្រាក់សរុបដែលត្រូវបង់:</th>
                                <td><?php echo "$". $totalpayment ?></td>
                            </tr>
                        </table>
                    </div>
                    <div class="card-footer">
                        <button class="btn btn-dark" id="show" style="--bs-btn-padding-y: .25rem; --bs-btn-padding-x: .5rem; --bs-btn-font-size: .75rem;">បង្ហាញ</button>
                        <button class="btn btn-danger" id="hide" style="--bs-btn-padding-y: .25rem; --bs-btn-padding-x: .5rem; --bs-btn-font-size: .75rem;">លាក់</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container text-center">
            <table class="table table-bordered border-dark" id="table" style="width: 1230px;position: relative;top: 10px;height: auto;display: flexbox;align-items: center;justify-content: center;">
                <thead class="table-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Payment</th>
                        <th scope="col">Principal</th>
                        <th scope="col">Interest</th>
                        <th scope="col">Balance</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($lists as $payment):?>
                        <tr>
                            <td><?php echo $payment['month']?></td>
                            <td><?php echo number_format($payment['payment'],2)?></td>
                            <td><?php echo number_format($payment['principal'],2)?></td>
                            <td><?php echo number_format($payment['interest'],2) ?></td>
                            <td><?php echo number_format($payment['balance'],2)?></td>
                        </tr>
                    <?php endforeach;?>
                   
                </tbody>
            </table>
    </div>
    </div>
</body>
<script>
    $("#table").hide();
    $("#hide").hide();
    $(document).ready(function() {
        $('#year').on('input', function() {
            var year = $(this).val();
            var month = year * 12;
            $("#month").val(month);
        }) 
    });
    $('#show').on('click', function() {
        $('#table').show();
        $('#hide').show();
        $('#hide').on('click', function() {
            $('#hide').hide();
            $('#table').hide();
        });
    });
</script>

</html>