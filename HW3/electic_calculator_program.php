<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    <title>electric program</title>
    <style>
        label{
            font-family: Battambang;
        }
        .container {
            position: relative;
            align-items: center;
            display: flex;
            justify-content: center;
            top: 100px;
        }
        .card {
            width: 1000px;
            height: 450px;
        }
        button{
            margin: 5px;
        }
    </style>
</head>

<body>
<?php
    try{
        $result = 0;$w = 0;$input_e = "";$p = 0;$t = 0;$price = 0;
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $p = $_POST["p"];
            $t = $_POST["t"];
            if(isset($_POST['btn_cal'])){
                if(empty($p) || empty($t)){
                    $input_e = "<label style='color:red;'>* Please enter the value on input !</label>";
                } else {
                    $w = $p * $t;
                    if($w < 500){
                        $result = $w * 500; $price = 500;
                    } else if ($w > 500 && $w <= 700){
                        $result = $w * 700; $price = 700;
                    } else if ($w > 700 && $w <= 1000){
                        $result = $w * 1000; $price = 1000;
                    } else if ($w > 1000){
                        $result = $w * 1500; $price = 1500;
                    }
                }
            }
        }
    } catch (Exception $e){
        echo "error = >".$e;
    }
?>
    <div class="container">
        <form method="POST">
           <?php echo $input_e?>
            <div class="card border-dark">
                <div class="card-header">
                    <h5>2-Electric calculator program</h5>
                </div>
                <div class="card-body">
                    <div class="mb-3">
                        <label for="formGroupExampleInput" class="form-label" require>អនុភាព(P):</label>
                        <input type="number|other" class="form-control" id="formGroupExampleInput" name="p" placeholder="power" value="<?php echo $p?>">
                    </div>
                    <div class="mb-3">
                        <label for="formGroupExampleInput2" class="form-label">រយះពេលប្រើប្រាស់(T):</label>
                        <input type="number" class="form-control" id="formGroupExampleInput2" name="t" placeholder="time" value="<?php echo $t?>">
                    </div>
                    <div class="mb-3">
                        <label for="formGroupExampleInput2" class="form-label">តម្លៃអគ្គីសនី(W):</label>
                        <input type="number" class="form-control" id="formGroupExampleInput2" placeholder="w" name="w" value="<?php echo $price?>">
                    </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-success" style="background-image: linear-gradient(to bottom , red , yellow);" type="submit" name="btn_cal" id="btn_cal">
                        <label>គណនាថាមពលអគ្គីសនី</label>
                    </button>    
                    <label>តម្លៃថាមពលអគ្គីសនី​​ <?php echo $price. "/"  . $w . "kwh"."=" . $result. "​ រៀល" ?> </label>
                </div>
            </div>
        </form>
    </div>
</body>
</html>