<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    <title>Array to string</title>
    <style>
        label {
            font-family: Battambang;
        }

        .container {
            position: relative;
            align-items: center;
            display: flex;
            justify-content: center;
            top: 50px;
        }

        .card {
            width: 1000px;
            height: 450px;
            background: none;
        }
    </style>
</head>
<body>
    <?php
        try{
            $e = "";$t="";$colors =[];
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                if (empty($_POST['name'])) {
                    $e = "*name request";
                } else {
                    if(!empty($_POST['color'])){
                        $colors = $_POST['color'];
                        $t = $_POST['name']."'s"." favourite colors are " . count($colors) . " option(s)"; 
                    } else {
                        $colors = array();
                        $e = "*please choose the option";
                    }
                }
            }
        }catch(Exception $er){
            echo "error =>".$er;
        }
    ?>
    <div class="container">
        <form method="POST">
            <label style="color:red;"><?php echo $e ?></label>
            <div class="card border-dark">
                <div class="card-header">
                    <h4>4-Array to string conversion</h4>
                </div>
                <div class="card-body">
                    <div class="mb-3">
                        <label for="formGroupExampleInput" class="form-label">Name</label>
                        <input type="text" class="form-control" id="formGroupExampleInput" placeholder="name" name="name" value="<?php if(!empty($_POST['name'])) echo $_POST['name']?>">
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="red" id="flexCheckDefault" name="color[]" <?php if(in_array("red",$colors)) echo "checked";?>>
                        <label class="form-check-label" for="flexCheckDefault">
                            red
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="blue" id="flexCheckDefault" name="color[]" <?php if(in_array("blue",$colors)) echo "checked";?>>
                        <label class="form-check-label" for="flexCheckDefault">
                            blue
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="green" id="flexCheckDefault" name="color[]" <?php if(in_array("green",$colors)) echo "checked";?>>
                        <label class="form-check-label" for="flexCheckDefault">
                            green
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="yellow" id="flexCheckDefault" name="color[]" <?php if(in_array("yellow",$colors)) echo "checked";?>>
                        <label class="form-check-label" for="flexCheckDefault">
                            yellow
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="pink" id="flexCheckDefault" name="color[]" <?php if(in_array("pink",$colors)) echo "checked";?>>
                        <label class="form-check-label" for="flexCheckDefault">
                            pink
                        </label>
                    </div>
                    <div class="row">
                        <label>
                            <?php echo $t?>
                        </label>
                    </div>
                    <div class="row">
                        <?php foreach($colors as $color):?>
                            <div style="width: 100px;height:30px;margin:10px;color:gray; 5px; background: <?php echo $color?>;"><?php echo $color?></div>
                        <?php endforeach;?>
                    </div>
                    
                </div>
                <div class="card-footer">
                    <button class="btn btn-dark" type="submit">submit</button>
                </div>
            </div>
        </form>
    </div>
</body>
</html>