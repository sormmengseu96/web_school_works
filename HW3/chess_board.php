<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    <title>Chess board</title>
    <style>
        label {
            font-family: Battambang;
        }

        .container {
            position: relative;
            align-items: center;
            display: flex;
            justify-content: center;
            top: 50px;
        }

        .card {
            width: auto;
            height: auto;
        }
        
    </style>
</head>
<body>
    <div class="container">
        <div class="card border-dark">
            <div class="card-header">
                <h5>3-Using nested for loop create chess board</h5>
            </div>
            <div class="card-body">
                <table width="570px" cellspacing="0px" cellpadding="0px" border="1px">
                    <?php
                        for ($row = 1; $row <= 8; $row++) {
                            echo "<tr>";
                            for ($col = 1; $col <= 8; $col++) {
                                $total = $row + $col;
                                if ($total % 2 == 0) {
                                    echo "<td height=70px width=70px bgcolor=#FFFFFF></td>";
                                } else {
                                    echo "<td style='height=70px; width=70px; background-image: linear-gradient(to bottom , red , yellow);'></td>";
                                }
                            }
                            echo "</tr>";
                        }
                    ?>
                </table>
            </div>
        </div>
    </div>
</body>
</html>