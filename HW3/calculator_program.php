<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    <title>calculator program</title>
    <style>
        .container {
            position: relative;
            align-items: center;
            display: flex;
            justify-content: center;
            top: 100px;
        }
        .card {
            width: 1000px;
            height: 400px;
        }
    </style>
</head>
<body>
<?php
    try{
        $first_number = 0;$second_number = 0;$result = 0;
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $first_number = $_POST['first_number'];$second_number = $_POST['second_number'];
            if(empty($first_number) || empty($second_number)){
                $result = "is invalid";
            } else {
                if(isset($_POST['btn_add'])){
                    $result = $first_number + $second_number;
                } else if (isset($_POST['btn_subtract'])){
                    $result = $first_number - $second_number;
                } else if(isset($_POST['btn_multiply'])){
                    $result = $first_number * $second_number;
                } else if(isset($_POST['btn_devide'])){
                    $result = $first_number / $second_number;
                } 
            }
        }
    } catch (Exception $e){
        echo "error = >".$e;
    }
?>
    <div class="container">
        <form method="POST">
            <div class="card border-dark">
                <div class="card-header">
                    <h3>1-Calculator program using operators</h3>
                </div>
                <div class="card-body">
                    <div class="mb-3">
                        <label for="formGroupExampleInput" class="form-label" require>First Number</label>
                        <input type="number" class="form-control" id="formGroupExampleInput" placeholder="please input first number" name="first_number" value="<?php echo $first_number?>">
                    </div>
                    <div class="mb-3">
                        <label for="formGroupExampleInput2" class="form-label">Second Number</label>
                        <input type="number" class="form-control" id="formGroupExampleInput2" placeholder="please input second number" name="second_number" value="<?php echo $second_number?>">
                    </div>
                    <div class="mb-3">
                        <label for="formGroupExampleInput2" class="form-label">Result</label>
                        <input type="number" class="form-control" id="formGroupExampleInput2" placeholder="result" name="result" value="<?php echo $result?>">
                    </div>
                </div>
                <div class="card-footer">
                    <button class="btn btn-dark" type="submit" name="btn_add" id="btn_add">Add</button>
                    <button class="btn btn-dark" type="submit" name="btn_subtract" id="btn_subtract">Subtract</button>
                    <button class="btn btn-dark" type="submit" name="btn_multiply" id="btn_multiply">Multiply</button>
                    <button class="btn btn-dark" type="submit" name="btn_devide" id="btn_devide">Devide</button>
                </div>
            </div>
        </form>
    </div>
</body>
</html>